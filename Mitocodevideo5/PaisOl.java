/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mitocodevideo5;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Esteban
 */
public class PaisOl {
   public static PaisOl instancia = null;
	private List paises = null;
	
	private PaisOl(){
		
	}
	
	public static PaisOl getInstance(){
		if(instancia == null){
			instancia = new PaisOl();
		}
		return instancia;
	}	

	public List getPaises() {
		if (paises == null) {
			paises = new ArrayList();
			
			Pais p1 = new Pais("PERU");
			Pais p2 = new Pais("MEXICO");
			Pais p3 = new Pais("COLOMBIA");

			paises.add(p1);
			paises.add(p2);
			paises.add(p3);
		}
		return paises;
	} 
}
