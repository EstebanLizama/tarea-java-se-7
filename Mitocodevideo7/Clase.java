/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Mitocodevideo7;

import Mitocodevideo6.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Esteban
 */
public class Clase<T,K,V,E> {
    private T objetoT;
    private K objetoK;
    private V objetoV;
    private E objetoE;

    public Clase(T objetoT, K objetoK, V objetoV, E objetoE) {
        this.objetoT = objetoT;
        this.objetoK = objetoK;
        this.objetoV = objetoV;
        this.objetoE = objetoE;
    }

  
    public void mostrarTipo(){
        System.out.println(""+objetoT.getClass().getName());
        System.out.println(""+objetoK.getClass().getName());
        System.out.println(""+objetoV.getClass().getName());
        System.out.println(""+objetoE.getClass().getName());
    }
   
   
    
}
